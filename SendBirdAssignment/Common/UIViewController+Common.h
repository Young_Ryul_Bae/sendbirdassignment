@interface UIViewController (Common)

- (void)scrollWhenKeyboardIsShowing:(UIScrollView *)scrollview
                         activeView:(UIView *)activeView;

@end

