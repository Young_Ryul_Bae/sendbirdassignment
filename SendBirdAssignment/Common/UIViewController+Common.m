#import <objc/runtime.h>
#import "UIViewController+Common.h"

@implementation UIViewController (Common)

#pragma mark - Scroll When Keyboard Appears

- (void)_setDictionaryForScrollWhenKeyboardAppears:(NSMutableDictionary *)dictinoaryForScrollWhenKeyboardAppears {
    objc_setAssociatedObject(self, @selector(_dictinoaryForScrollWhenKeyboardAppears), dictinoaryForScrollWhenKeyboardAppears, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSMutableDictionary *)_dictinoaryForScrollWhenKeyboardAppears {
    return objc_getAssociatedObject(self, @selector(_dictinoaryForScrollWhenKeyboardAppears));
}

- (void)scrollWhenKeyboardIsShowing:(UIScrollView *)scrollview
                         activeView:(UIView *)activeView {
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:scrollview forKey:@"scrollView"];
    [dict setObject:activeView forKey:@"activeView"];
    [dict setObject:[NSNumber numberWithDouble:(double)scrollview.contentInset.bottom] forKey:@"originalInsetBottom"];
    [dict setObject:[NSNumber numberWithDouble:(double)scrollview.contentInset.top] forKey:@"originalInsetTop"];
    [dict setObject:[NSNumber numberWithDouble:(double)scrollview.contentInset.left] forKey:@"originalInsetLeft"];
    [dict setObject:[NSNumber numberWithDouble:(double)scrollview.contentInset.right] forKey:@"originalInsetRight"];
    [self _setDictionaryForScrollWhenKeyboardAppears:dict];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(_keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(_keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)_keyboardWasShown:(NSNotification*)aNotification {
    UIScrollView *scrollView = [[self _dictinoaryForScrollWhenKeyboardAppears] valueForKey:@"scrollView"];
    UIView *activeView = [[self _dictinoaryForScrollWhenKeyboardAppears] valueForKey:@"activeView"];
    NSNumber *originalInsetBottom = [[self _dictinoaryForScrollWhenKeyboardAppears] valueForKey:@"originalInsetBottom"];
    NSNumber *originalInsetTop = [[self _dictinoaryForScrollWhenKeyboardAppears] valueForKey:@"originalInsetTop"];
    NSNumber *originalInsetLeft = [[self _dictinoaryForScrollWhenKeyboardAppears] valueForKey:@"originalInsetLeft"];
    NSNumber *originalInsetRight = [[self _dictinoaryForScrollWhenKeyboardAppears] valueForKey:@"originalInsetRight"];

    NSDictionary* info = [aNotification userInfo];
    // kbSize is wrong at first call. typical Apple bug?
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    CGFloat inputAccessoryViewOffset = 0.0;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake([originalInsetTop doubleValue],
                                                  [originalInsetLeft doubleValue],
                                                  [originalInsetBottom doubleValue] + kbSize.height + inputAccessoryViewOffset,
                                                  [originalInsetRight doubleValue]);
    scrollView.contentInset = contentInsets;
    scrollView.scrollIndicatorInsets = contentInsets;
    
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbSize.height;
    aRect.size.height -= inputAccessoryViewOffset;
    if (!CGRectContainsPoint(aRect, activeView.frame.origin) ) {
        
        CGPoint scrollPoint = CGPointMake(0.0, activeView.frame.origin.y - kbSize.height + inputAccessoryViewOffset);
        
        [scrollView setContentOffset:scrollPoint animated:YES];
    }
}

- (void)_keyboardWillBeHidden:(NSNotification*)aNotification {
    UIScrollView *scrollView = [[self _dictinoaryForScrollWhenKeyboardAppears] valueForKey:@"scrollView"];
    NSNumber *originalInsetBottom = [[self _dictinoaryForScrollWhenKeyboardAppears] valueForKey:@"originalInsetBottom"];
    NSNumber *originalInsetTop = [[self _dictinoaryForScrollWhenKeyboardAppears] valueForKey:@"originalInsetTop"];
    NSNumber *originalInsetLeft = [[self _dictinoaryForScrollWhenKeyboardAppears] valueForKey:@"originalInsetLeft"];
    NSNumber *originalInsetRight = [[self _dictinoaryForScrollWhenKeyboardAppears] valueForKey:@"originalInsetRight"];
    UIEdgeInsets contentInsets = UIEdgeInsetsMake([originalInsetTop doubleValue], [originalInsetLeft doubleValue], [originalInsetBottom doubleValue], [originalInsetRight doubleValue]);
    scrollView.contentInset = contentInsets;
    scrollView.scrollIndicatorInsets = contentInsets;
}

@end
