#import <objc/runtime.h>
#import "UIImageView+Common.h"
#import "ImageCache.h"

@implementation UIImageView (Common)

- (void)setUrlString:(NSString *)urlString {
    objc_setAssociatedObject(self, @selector(urlString), urlString, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSString *)urlString {
    return objc_getAssociatedObject(self, @selector(urlString));
}

- (void)loadImageByUrl:(NSString *)urlString {
    if([NSString isNilOrEmpty:urlString]) {
        return;
    }
    ImageCache *cache = [ImageCache shard];
    if([cache hasImageByUrl:urlString]) {
        self.image = [cache getImageByUrl:urlString];
        return;
    }
    
    self.image = nil;
    self.urlString = urlString;
    NSURL *url = [NSURL URLWithString:urlString];
    __weak typeof(self) weakSelf = self;
    NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url
                                                         completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (data) {
            UIImage *image = [UIImage imageWithData:data];
            if (image) {
                [cache addImage:image
                         forUrl:response.URL.absoluteString];
                
                if([weakSelf.urlString isEqualToString:response.URL.absoluteString]) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        weakSelf.image = image;
                    });
                }
            }
        }
    }];
    [task resume];
}

@end
