
@interface ImageCache : NSObject

+ (instancetype)shard;

- (void)addImage:(UIImage *)image forUrl:(NSString *)url;
- (BOOL)hasImageByUrl:(NSString *)url;
- (UIImage *)getImageByUrl:(NSString *)url;

@end
