@class BookModel;
@class BookDetailT;

@interface BookCollectionViewCell : UICollectionViewCell

+ (NSString *)identifier;
+ (CGSize)size;

@property (strong, nonatomic) BookModel *data;
@property (strong, nonatomic) BookDetailT *detailTData;

+ (BookCollectionViewCell *)cell;


@end
