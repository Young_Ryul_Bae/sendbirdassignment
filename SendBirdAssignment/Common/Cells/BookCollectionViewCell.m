#import "BookCollectionViewCell.h"
#import "SearchModel.h"
#import "BookDetailT+CoreDataProperties.h"

@interface BookCollectionViewCell ()

@property (weak, nonatomic) IBOutlet UIImageView *image;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *subTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *isbnLabel;

@end

@implementation BookCollectionViewCell

+ (NSString *)identifier {
    return @"BookCollectionViewCell";
}

+ (CGSize)size {
    CGFloat width = [Utils getDeviceWidth] / 2 - 8;
    CGFloat height = width + 168;
    return CGSizeMake(width, height);
}

+ (BookCollectionViewCell *)cell {
    return (BookCollectionViewCell *)[NSBundle.mainBundle loadNibNamed:@"BookCollectionViewCell"
                                                                 owner:nil
                                                               options:nil][0];
}

- (void)setData:(BookModel *)data {
    if(data == nil) {
        return;
    }
    [self.image loadImageByUrl:data.image];
    self.titleLabel.text = data.title;
    self.subTitleLabel.text = data.subtitle;
    self.priceLabel.text = data.price;
    self.isbnLabel.text = data.isbn13;
}

- (void)setDetailTData:(BookDetailT *)detailTData {
    if(detailTData == nil) {
        return;
    }
    
    [self.image loadImageByUrl:detailTData.image];
    self.titleLabel.text = detailTData.title;
    self.subTitleLabel.text = detailTData.subtitle;
    self.priceLabel.text = detailTData.price;
    self.isbnLabel.text = detailTData.isbn13;
}

- (void)awakeFromNib {
    [super awakeFromNib];
}

@end
