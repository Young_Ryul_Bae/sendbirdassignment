#import "Utils.h"

@implementation Utils

+ (void)showAlertWithTitle:(NSString *)title
                       msg:(NSString *)msg
            viewController:(UIViewController *)viewController {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title
                                                                   message:msg preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"Okay!"
                                              style:UIAlertActionStyleDefault
                                            handler:nil]];
    [viewController presentViewController:alert
                                 animated:YES
                               completion:nil];
}

+ (void)defaultErrorAlert:(NSError *)error
           viewController:(UIViewController *)viewController {
    [Utils showAlertWithTitle:@"Our Fault!"
                          msg: [NSString stringWithFormat: @"Something Went Wrong\n%@", [error localizedDescription]]
               viewController:viewController];
}

+ (CGFloat)getDeviceWidth {
    return [UIScreen mainScreen].bounds.size.width;
}

@end
