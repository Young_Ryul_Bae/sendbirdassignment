#import "NSString+Common.h"

@implementation NSString (Common)

+ (BOOL)isNilOrEmpty:(NSString *)string {
    return string == nil || [string length] ==0;
}

@end
