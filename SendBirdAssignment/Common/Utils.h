@class AppDelegate;

@interface Utils : NSObject

+ (void)showAlertWithTitle:(NSString *)title
                       msg:(NSString *)msg
            viewController:(UIViewController *)viewController;

+ (void)defaultErrorAlert:(NSError *)error
           viewController:(UIViewController *)viewController;

+ (CGFloat)getDeviceWidth;

@end
