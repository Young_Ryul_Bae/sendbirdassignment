#import "UITextView+Common.h"

@implementation UITextView (UITextView_Common)

- (void)addKeyboardSaveAndCloseButtonWithSaveCallback:(SEL)saveSelector
                                               target:(id)target {
    if(![target respondsToSelector:saveSelector]) {
        return;
    }
    UIBarButtonItem *saveButton = [[UIBarButtonItem alloc] initWithTitle:@"Save"
                                                                   style:UIBarButtonItemStyleDone
                                                                  target:target
                                                                  action:saveSelector];
    
    UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                          target:self
                                                                          action:nil];
    
    
    UIBarButtonItem *closeButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                                                 target:self
                                                                                 action:@selector(resignFirstResponder)];
    
    UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, [Utils getDeviceWidth], 44)];
    toolbar.items = @[saveButton, flex, closeButton];
    self.inputAccessoryView = toolbar;
}

- (void)addKeyboardCloseButton {
    UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                          target:self
                                                                          action:nil];

    UIBarButtonItem *closeButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                                                 target:self
                                                                                 action:@selector(resignFirstResponder)];
    UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, [Utils getDeviceWidth], 44)];
    toolbar.items = @[flex, closeButton];
    self.inputAccessoryView = toolbar;
}

@end
