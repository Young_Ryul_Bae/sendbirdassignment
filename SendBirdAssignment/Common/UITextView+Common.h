@interface UITextView (UITextView_Common)

- (void)addKeyboardSaveAndCloseButtonWithSaveCallback:(SEL)saveSelector
                                               target:(id)target;
- (void)addKeyboardCloseButton;

@end
