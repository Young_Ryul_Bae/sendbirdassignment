
@interface UIImageView (Common)

@property (strong, nonatomic) NSString *urlString;

- (void)loadImageByUrl:(NSString *)url;

@end
