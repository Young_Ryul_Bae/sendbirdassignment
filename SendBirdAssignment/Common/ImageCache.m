#import "ImageCache.h"

@interface ImageCache ()

@property (strong, nonatomic) NSMutableDictionary<NSString *, UIImage *> *cache;

@end

@implementation ImageCache

+ (instancetype)shard {
    static ImageCache *cache = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        cache = [[self alloc] init];
    });
    return cache;
}

- (id)init {
  if (self = [super init]) {
      self.cache = [[NSMutableDictionary alloc] init];
  }
  return self;
}

- (void)addImage:(UIImage *)image forUrl:(NSString *)url {
    [self.cache setValue:image forKey:url];
}

- (BOOL)hasImageByUrl:(NSString *)url {
    return [self.cache objectForKey:url] != nil;
}

- (UIImage *)getImageByUrl:(NSString *)url {
    return [self.cache objectForKey:url];
}

@end
