#import "DetailViewController.h"
#import "DaoClient.h"
#import "BookDetailT+CoreDataProperties.h"

@interface DetailViewController ()
< DaoClientDelegate
, UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingIndicator;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIImageView *image;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *subTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *authorLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *miscellaneousInfoLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *rateStarWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *rateColorViewWidthConstraint;
@property (weak, nonatomic) IBOutlet UILabel *descLabel;
@property (weak, nonatomic) IBOutlet UITextView *noteTextView;

@property (strong, nonatomic) DaoClient *dao;
@property (strong, nonatomic) BookDetailT *data;

@end

@implementation DetailViewController

#pragma mark - Class Methods

+ (DetailViewController *)viewControllerWithIsbn:(NSString *)isbn {
    DetailViewController *detailViewController =  [[DetailViewController alloc] initWithNibName:@"DetailViewController"
                                                                                         bundle:nil];
    detailViewController.isbn = isbn;
    return detailViewController;
}

#pragma mark - Private Methods
- (void)requestApi {
    if([NSString isNilOrEmpty:self.isbn]) {
        return;
    }
    [self.dao getBookDetailTByIsbn:self.isbn];
}

- (void)saveNote {
    if([NSString isNilOrEmpty:self.noteTextView.text]) {
        [Utils showAlertWithTitle:@"Warning!"
                              msg:@"Type In the Note."
                   viewController:self];
        return;
    }
    [self.dao saveUserNote:self.noteTextView.text
                  toIsbn13:self.data.isbn13];
    [self.noteTextView resignFirstResponder];
}

#pragma mark - Setters
- (void)setData:(BookDetailT *)data {
    _data = data;
    [self.image loadImageByUrl:data.image];
    self.titleLabel.text = data.title;
    self.subTitleLabel.text = data.subtitle;
    self.authorLabel.text = data.authors;
    self.priceLabel.text = data.price;
    NSString *misc = [NSString stringWithFormat:
                      @"Publisher : %@\n"
                      "Language : %@\n"
                      "ISBN : %@\n"
                      "ISBN13 : %@\n"
                      "Pages :  %@\n"
                      "Year : %@"
                      , data.publisher, data.language, data.isbn10, data.isbn13, data.pages, data.year];
    self.miscellaneousInfoLabel.text = misc;
    NSInteger rate = [data.rating intValue];
    self.rateColorViewWidthConstraint.constant = MAX(self.rateStarWidthConstraint.constant / 5 * rate, 1);
    self.descLabel.text = data.desc;
    self.noteTextView.text = data.usernote;
}

#pragma mark - Action Events
- (IBAction)moreInfoTouched:(UIButton *)sender {
    NSURL *url = [NSURL URLWithString:self.data.url];
    if(url == nil) {
        return;
    }
    if([[UIApplication sharedApplication] canOpenURL:url]) {
        [[UIApplication sharedApplication] openURL:url
                                           options:@{}
                                 completionHandler:nil];
    }
}

#pragma mark - Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    self.dao = [[DaoClient alloc] initWithContainer:[AppDelegate getAppDelegate].persistentContainer];
    self.dao.delegate = self;
    [self requestApi];
    [self.noteTextView addKeyboardSaveAndCloseButtonWithSaveCallback:@selector(saveNote)
                                                              target:self];
    [self scrollWhenKeyboardIsShowing:self.scrollView
                           activeView:self.noteTextView];
    
    self.title = @"Detail";
}

#pragma mark - DaoClientDelegate
- (void)receivedGetBookDetailTByIsbn:(NSString *)isbn
                                data:(BookDetailT *)data {
    [self.loadingIndicator setHidden:YES];
    self.data = data;
    [self.scrollView setHidden:NO];
}

- (void)errorGetBookDetailByIsbn:(NSString *)isbn
                           error:(NSError *)error {
    [self.loadingIndicator setHidden:YES];
    [Utils defaultErrorAlert:error
              viewController:self];
}


@end
