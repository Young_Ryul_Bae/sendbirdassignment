@interface DetailViewController : UIViewController

@property (strong, nonatomic) NSString *isbn;

+ (DetailViewController *)viewControllerWithIsbn:(NSString *)isbn;

@end
