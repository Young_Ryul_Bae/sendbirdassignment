#import "MyNoteViewController.h"
#import "DaoClient.h"
#import "BookCollectionViewCell.h"
#import "BookDetailT+CoreDataProperties.h"
#import "DetailViewController.h"

@interface MyNoteViewController ()
<DaoClientDelegate
, UICollectionViewDataSource
, UICollectionViewDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (strong, nonatomic) DaoClient *dao;
@property (strong, nonatomic) NSArray<BookDetailT *> *collectionViewList;

@end

@implementation MyNoteViewController

#pragma mark - Class Methods
+ (MyNoteViewController *)viewController {
    return [[MyNoteViewController alloc] initWithNibName:@"MyNoteViewController"
                                                  bundle:nil];
}

#pragma mark - Private Methods
- (void)registerCells {
    [self.collectionView registerNib:[UINib nibWithNibName:[BookCollectionViewCell identifier] bundle:nil]
          forCellWithReuseIdentifier:[BookCollectionViewCell identifier]];
}

#pragma mark - Setters
- (void)setCollectionViewList:(NSArray<BookDetailT *> *)collectionViewList {
    _collectionViewList = collectionViewList;
    [self.collectionView reloadData];
}

#pragma mark - Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    self.dao = [[DaoClient alloc] initWithContainer:[AppDelegate getAppDelegate].persistentContainer];
    self.dao.delegate = self;
    [self registerCells];
    self.collectionViewList = [[NSMutableArray alloc] init];
    UICollectionViewFlowLayout *flowLayout = (UICollectionViewFlowLayout *)self.collectionView.collectionViewLayout;
    [flowLayout setItemSize:[BookCollectionViewCell size]];
    [self.dao getAllBookDetailT];
    self.title = @"My Notes";
}

#pragma mark - DaoClientDelegate
- (void)receivedGetAllBookDetailT:(NSArray<BookDetailT *> *)data {
    self.collectionViewList = data;
}

#pragma mark - UICollectionViewDataSource & Delegate
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView
     numberOfItemsInSection:(NSInteger)section {
    return [self.collectionViewList count];
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                           cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    BookCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[BookCollectionViewCell identifier]
                                                                             forIndexPath:indexPath];
    cell.detailTData = self.collectionViewList[indexPath.row];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView
didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSString *isbn = [self.collectionViewList objectAtIndex:indexPath.row].isbn13;
    RootNavigationController *navi = [AppDelegate getRootNaviController];
    [navi pushViewController:[DetailViewController viewControllerWithIsbn:isbn]
                    animated:YES];
}

@end
