#import <CoreData/CoreData.h>
@class RootNavigationController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

+ (AppDelegate *)getAppDelegate;
+ (RootNavigationController *)getRootNaviController;

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) RootNavigationController *rootNavigationController;

@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (void)saveContext;


@end

