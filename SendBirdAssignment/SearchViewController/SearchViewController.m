#import "SearchViewController.h"
#import "SearchModel.h"
#import "DaoClient.h"
#import "BookCollectionViewCell.h"
#import "DetailViewController.h"
#import "MyNoteViewController.h"

@interface SearchViewController ()
< DaoClientDelegate
, UICollectionViewDataSource
, UICollectionViewDelegate
, UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *searchHeaderHeight;
@property (weak, nonatomic) IBOutlet UITextField *searchTextField;
@property (weak, nonatomic) IBOutlet UIButton *searchButton;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingIndicator;

@property (strong, nonatomic) DaoClient *dao;
@property (strong, nonatomic) SearchModel *data;
@property (strong, nonatomic) NSMutableArray<BookModel *> *collectionViewList;
@property (assign, nonatomic) NSInteger nextPage;
@property (strong, nonatomic) NSString *keyword;

@end

@implementation SearchViewController

#pragma mark - Class Methods
+ (SearchViewController *)viewController {
    return [[SearchViewController alloc] initWithNibName:@"SearchViewController"
                                                  bundle:nil];
}

#pragma mark - Private Methods
- (void)registerCells {
    [self.collectionView registerNib:[UINib nibWithNibName:[BookCollectionViewCell identifier] bundle:nil]
          forCellWithReuseIdentifier:[BookCollectionViewCell identifier]];
}

- (void)requestApiByKeyword:(NSString *)keyword
                       page:(NSInteger)page {
    // when new keyword, we reset.
    if(page == 1) {
        [self.collectionViewList removeAllObjects];
    }
    [self.loadingIndicator setHidden:NO];
    self.keyword = keyword;
    [self.dao getBooksByKeyword:self.keyword
                           page:page];
}

- (void)myNoteTouched {
    [[AppDelegate getRootNaviController] pushViewController:[MyNoteViewController viewController]
                                                   animated:YES];
}

#pragma mark - Setters
- (void)setData:(SearchModel *)data {
    _data = data;
    [self.collectionViewList addObjectsFromArray:data.books];
    [self.collectionView reloadData];
    // when first page load, scroll to top.
    if(self.nextPage == 2) {
        [self.collectionView setContentOffset:CGPointZero];
    }
}

#pragma mark - Action Events
- (IBAction)searchButtonTouched:(UIButton *)sender {
    if([self.searchTextField.text length] == 0) {
        [Utils showAlertWithTitle:@"Alert!"
                              msg:@"Please, Type in the Words."
                   viewController:self];
    }
    if([self.searchTextField.text isEqualToString:self.keyword]) {
        return;
    }

    [self requestApiByKeyword:self.searchTextField.text
                         page:1];
}

#pragma mark - Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    self.dao = [[DaoClient alloc] initWithContainer:[AppDelegate getAppDelegate].persistentContainer];
    self.dao.delegate = self;
    [self registerCells];
    self.collectionViewList = [[NSMutableArray alloc] init];
    UICollectionViewFlowLayout *flowLayout = (UICollectionViewFlowLayout *)self.collectionView.collectionViewLayout;
    [flowLayout setItemSize:[BookCollectionViewCell size]];
    [self.searchTextField addKeyboardCloseButton];
    
    self.title = @"Search";
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"My Notes"
                                                                             style:UIBarButtonItemStylePlain
                                                                            target:self
                                                                            action:@selector(myNoteTouched)];
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    [self.searchButton sendActionsForControlEvents: UIControlEventTouchUpInside];
    return NO;
}

#pragma mark - DaoClientDelegate
- (void)receivedGetBooksByKeyword:(NSString *)keyword
                             page:(NSInteger)page
                             data:(SearchModel *)data {
    [self.loadingIndicator setHidden:YES];
    self.nextPage = page + 1;
    self.data = data;
}

- (void)errorGetBooksByKeyword:(NSString *)keyword
                          page:(NSInteger)page
                         error:(NSError *)error {
    [self.loadingIndicator setHidden:YES];
    [Utils defaultErrorAlert:error
              viewController:self];
}

#pragma mark - UICollectionViewDataSource & Delegate
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView
     numberOfItemsInSection:(NSInteger)section {
    return [self.collectionViewList count];
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                           cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    BookCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[BookCollectionViewCell identifier]
                                                                             forIndexPath:indexPath];
    cell.data = self.collectionViewList[indexPath.row];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView
       willDisplayCell:(UICollectionViewCell *)cell
    forItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == self.collectionViewList.count - 2 ) {
        if(self.nextPage <= ceil([self.data.total doubleValue] / [DaoClient defaultPageSize])) {
            [self requestApiByKeyword:self.keyword
                                 page:self.nextPage];
        }
    }
}

- (void)collectionView:(UICollectionView *)collectionView
didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSString *isbn = [self.collectionViewList objectAtIndex:indexPath.row].isbn13;
    RootNavigationController *navi = [AppDelegate getRootNaviController];
    [navi pushViewController:[DetailViewController viewControllerWithIsbn:isbn]
                    animated:YES];
}

- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView
                     withVelocity:(CGPoint)velocity
              targetContentOffset:(inout CGPoint *)targetContentOffset {
    if (velocity.y > 0){
        [self.view layoutIfNeeded];
        [UIView animateWithDuration:0.2 delay:0.0
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
            self.searchHeaderHeight.constant = 0;
            [self.view layoutIfNeeded];
        } completion:nil];
    }
    if (velocity.y < 0){
        [self.view layoutIfNeeded];
        [UIView animateWithDuration:0.2 delay:0.0
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
            self.searchHeaderHeight.constant = 64;
            [self.view layoutIfNeeded];
        } completion:nil];
    }
}

@end
