//
//  BookDetailT+CoreDataProperties.h
//  SendBirdAssignment
//
//  Created by Young Ryul Bae on 2020/05/09.
//  Copyright © 2020 Young Ryul Bae. All rights reserved.
//
//

#import "BookDetailT+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface BookDetailT (CoreDataProperties)

+ (NSFetchRequest<BookDetailT *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *error;
@property (nullable, nonatomic, copy) NSString *title;
@property (nullable, nonatomic, copy) NSString *subtitle;
@property (nullable, nonatomic, copy) NSString *authors;
@property (nullable, nonatomic, copy) NSString *publisher;
@property (nullable, nonatomic, copy) NSString *language;
@property (nullable, nonatomic, copy) NSString *pages;
@property (nullable, nonatomic, copy) NSString *year;
@property (nullable, nonatomic, copy) NSString *rating;
@property (nullable, nonatomic, copy) NSString *desc;
@property (nullable, nonatomic, copy) NSString *price;
@property (nullable, nonatomic, copy) NSString *image;
@property (nullable, nonatomic, copy) NSString *url;
@property (nullable, nonatomic, copy) NSString *usernote;
@property (nullable, nonatomic, copy) NSString *isbn10;
@property (nullable, nonatomic, copy) NSString *isbn13;

@end

NS_ASSUME_NONNULL_END
