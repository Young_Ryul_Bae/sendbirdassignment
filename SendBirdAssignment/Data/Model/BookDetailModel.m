#import "BookDetailModel.h"

@implementation BookDetailModel

+ (BookDetailModel *)convertFromJsonDictionary:(NSDictionary *)dictionary {
    BookDetailModel *_self = [[BookDetailModel alloc] init];
    if(_self != nil) {
        @try {
            [_self setValuesForKeysWithDictionary:dictionary];
        }
        @catch (NSException *e) {
            
        }
    }
    return _self;
}

- (NSString*)description {
    return [NSString stringWithFormat:@"{\nauthors:%@\ndesc:%@\nerror:%@\nimage:%@\nisbn10:%@\nisbn13:%@\nlanguage:%@\npages:%@\nprice:%@\npublisher:%@\npages:%@\nsubtitle:%@\ntitle:%@\nurl:%@\nyear:%@\nurl:\n}", self.authors, self.desc, self.error, self.image, self.isbn10, self.isbn13, self.language, self.pages, self.price, self.publisher, self.rating, self.subtitle, self.title, self.url, self.year];
}
@end
