//
//  BookDetailT+CoreDataProperties.m
//  SendBirdAssignment
//
//  Created by Young Ryul Bae on 2020/05/09.
//  Copyright © 2020 Young Ryul Bae. All rights reserved.
//
//

#import "BookDetailT+CoreDataProperties.h"

@implementation BookDetailT (CoreDataProperties)

+ (NSFetchRequest<BookDetailT *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"BookDetailT"];
}

@dynamic error;
@dynamic title;
@dynamic subtitle;
@dynamic authors;
@dynamic publisher;
@dynamic language;
@dynamic pages;
@dynamic year;
@dynamic rating;
@dynamic desc;
@dynamic price;
@dynamic image;
@dynamic url;
@dynamic usernote;
@dynamic isbn10;
@dynamic isbn13;

@end
