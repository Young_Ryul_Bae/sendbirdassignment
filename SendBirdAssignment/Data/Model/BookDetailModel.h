
@interface BookDetailModel : NSObject

@property(strong, nonatomic) NSString *error;
@property(strong, nonatomic) NSString *title;
@property(strong, nonatomic) NSString *subtitle;
@property(strong, nonatomic) NSString *authors;
@property(strong, nonatomic) NSString *publisher;
@property(strong, nonatomic) NSString *language;
@property(strong, nonatomic) NSString *isbn10;
@property(strong, nonatomic) NSString *isbn13;
@property(strong, nonatomic) NSString *pages;
@property(strong, nonatomic) NSString *year;
@property(strong, nonatomic) NSString *rating;
@property(strong, nonatomic) NSString *desc;
@property(strong, nonatomic) NSString *price;
@property(strong, nonatomic) NSString *image;
@property(strong, nonatomic) NSString *url;

+ (BookDetailModel *)convertFromJsonDictionary:(NSDictionary *)dictionary;

@end
