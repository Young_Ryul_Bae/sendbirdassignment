
@interface BookModel : NSObject

@property(strong, nonatomic) NSString *title;
@property(strong, nonatomic) NSString *subtitle;
@property(strong, nonatomic) NSString *isbn13;
@property(strong, nonatomic) NSString *price;
@property(strong, nonatomic) NSString *image;
@property(strong, nonatomic) NSString *url;

+ (BookModel *)convertFromJsonDictionary:(NSDictionary *)dictionary;

@end

@interface SearchModel : NSObject

@property(strong, nonatomic) NSString *error;
@property(strong, nonatomic) NSString *total;
@property(strong, nonatomic) NSString *page;
@property(strong, nonatomic) NSArray<BookModel *> *books;

// this function 
+ (SearchModel *)convertFromJsonDictionary:(NSDictionary *)dictionary;

@end
