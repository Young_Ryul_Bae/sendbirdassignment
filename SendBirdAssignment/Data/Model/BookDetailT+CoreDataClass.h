//
//  BookDetailT+CoreDataClass.h
//  SendBirdAssignment
//
//  Created by Young Ryul Bae on 2020/05/09.
//  Copyright © 2020 Young Ryul Bae. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface BookDetailT : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "BookDetailT+CoreDataProperties.h"
