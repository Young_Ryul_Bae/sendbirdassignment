#import "SearchModel.h"

@implementation BookModel

+ (BookModel *)convertFromJsonDictionary:(NSDictionary *)dictionary {
    BookModel *_self = [[BookModel alloc] init];
    if(_self != nil) {
        [_self setValuesForKeysWithDictionary:dictionary];
    }
    return _self;
}

- (NSString*)description {
    return [NSString stringWithFormat:@"{\nimage:%@\nisbn13:%@\nprice:%@\nsubtitle:%@\ntitle:%@\nurl:%@\n\n}", self.image, self.isbn13, self.price, self.subtitle, self.title, self.url];
}

@end

@implementation SearchModel

+ (SearchModel *)convertFromJsonDictionary:(NSDictionary *)dictionary {
    SearchModel *_self = [[SearchModel alloc] init];
    if(_self != nil) {
        [_self setValuesForKeysWithDictionary:dictionary];
        NSMutableArray<BookModel *> *array = [[NSMutableArray alloc] init];
        for(NSDictionary *book in [dictionary valueForKey:@"books"]) {
            BookModel *bookModel = [BookModel convertFromJsonDictionary:book];
            [array addObject:bookModel];
        }
        _self.books = [array copy];
    }
    return _self;
}

- (NSString*)description {
    return [NSString stringWithFormat:@"{\nerror:%@\npage:%@\ntotal:%@\nbooks:%@\n}", self.error, self.page, self.total, self.books];
}

@end
