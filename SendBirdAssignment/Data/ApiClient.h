
@class SearchModel;
@class BookDetailModel;

@protocol ApiClientDelegate <NSObject>

@optional

- (void)receivedGetBooksByKeyword:(NSString *)keyword
                             page:(NSInteger)page
                             data:(SearchModel *)data;
- (void)errorGetBooksByKeyword:(NSString *)keyword
                          page:(NSInteger)page
                         error:(NSError *)error;

- (void)receivedGetBookDetailByIsbn:(NSString *)isbn
                               data:(BookDetailModel *)data;
- (void)errorGetBookDetailByIsbn:(NSString *)isbn
                           error:(NSError *)error;

@end


@interface ApiClient : NSObject

@property (nonatomic, weak) id <ApiClientDelegate> delegate;

- (void)getBooksByKeyword:(NSString *)keyword
                     page:(NSInteger)page;
- (void)getBookDetailByIsbn:(NSString *)isbn;

@end

