#import <CoreData/CoreData.h>
@class BookDetailT;
@class BookDetailModel;

@interface CoreDataClient : NSObject
@property (strong, nonatomic, readonly) NSManagedObjectContext *context;

- (instancetype)initWithContainer:(NSPersistentContainer *)container;

- (BookDetailT *)fetchBookDetailByIsbn13:(NSString *)isbn13;
- (NSArray<BookDetailT *> *)fetchAllBookDetailTWhereNoteIsNotNil;
- (BOOL)insertBookDetail:(BookDetailModel *)model;
- (BOOL)insertUserNote:(NSString *)note
              toIsbn13:(NSString *)isbn13;
- (BOOL)deleteBookDetailByIsbn:(NSString *)isbn13;

@end
