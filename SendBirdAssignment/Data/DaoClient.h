#import <CoreData/CoreData.h>
#import "ApiClient.h"
@class CoreDataClient;
@class BookDetailT;

typedef NS_ENUM(NSInteger, DataOrigin) {
    DataOriginNone,
    DataOriginFromInMemory,
    DataOriginFromCoreData,
    DataOriginFromWebApi
};

@protocol DaoClientDelegate <NSObject>

@optional
- (void)receivedGetBooksByKeyword:(NSString *)keyword
                             page:(NSInteger)page
                             data:(SearchModel *)data;

- (void)errorGetBooksByKeyword:(NSString *)keyword
                          page:(NSInteger)page
                         error:(NSError *)error;;

- (void)receivedGetBookDetailTByIsbn:(NSString *)isbn
                                data:(BookDetailT *)data;

- (void)errorGetBookDetailByIsbn:(NSString *)isbn
                           error:(NSError *)error;;

- (void)receivedGetAllBookDetailT:(NSArray<BookDetailT *> *)data;

@end


@interface DaoClient : NSObject <ApiClientDelegate>

@property (nonatomic, weak) id <DaoClientDelegate> delegate;

+ (NSInteger)defaultPageSize;

- (instancetype)initWithContainer:(NSPersistentContainer *)container;

- (DataOrigin)getBooksByKeyword:(NSString *)keyword
                           page:(NSInteger)page;
- (DataOrigin)getBookDetailTByIsbn:(NSString *)isbn;
- (void)getAllBookDetailT;

- (BOOL)saveBookDetail:(BookDetailModel *)bookDetail;
- (BOOL)saveUserNote:(NSString *)note
            toIsbn13:(NSString *)isbn13;
- (BOOL)removeBookDetailByIsbn13:(NSString *)isbn13;

@end
