#import "ApiClient.h"
#import "BookDetailModel.h"
#import "SearchModel.h"

@implementation ApiClient

- (void)getBooksByKeyword:(NSString *)keyword
                     page:(NSInteger)page {
    if(![self.delegate respondsToSelector:@selector(receivedGetBooksByKeyword:page:data:)]
       || keyword == nil) {
        return;
    }
    
    NSString *encodeKeyword = [keyword stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
    
    NSString *targetUrl = [NSString stringWithFormat:@"https://api.itbook.store/1.0/search/%@/%ld", encodeKeyword, (long)page];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setHTTPMethod:@"GET"];
    [request setURL:[NSURL URLWithString:targetUrl]];
    __weak typeof(self) weakSelf = self;
    [[[NSURLSession sharedSession] dataTaskWithRequest:request
                                     completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (error != nil) {
                if([weakSelf.delegate respondsToSelector:@selector(errorGetBooksByKeyword:page:error:)]) {
                    [weakSelf.delegate errorGetBooksByKeyword:keyword
                                                         page:page
                                                        error:error];
                }
                return;
            }
            NSError *err = nil;
            NSDictionary *JSONDictionary = [NSJSONSerialization JSONObjectWithData:data
                                                                           options:0
                                                                             error:&err];
            if (err != nil) {
                if([weakSelf.delegate respondsToSelector:@selector(errorGetBooksByKeyword:page:error:)]) {
                    [weakSelf.delegate errorGetBooksByKeyword:keyword
                                                         page:page
                                                        error:err];
                }
                return;
            }
            
            SearchModel *model = [SearchModel convertFromJsonDictionary:JSONDictionary];
            [weakSelf.delegate receivedGetBooksByKeyword:keyword
                                                    page:page
                                                    data:model];
        });
    }] resume];
}

- (void)getBookDetailByIsbn:(NSString *)isbn {
    if(![self.delegate respondsToSelector:@selector(receivedGetBookDetailByIsbn:data:)]
       || isbn == nil) {
        return;
    }
    
    NSString *encodeIsbn = [isbn stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
    
    NSString *targetUrl = [NSString stringWithFormat:@"https://api.itbook.store/1.0/books/%@", encodeIsbn];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setHTTPMethod:@"GET"];
    [request setURL:[NSURL URLWithString:targetUrl]];
    __weak typeof(self) weakSelf = self;
    [[[NSURLSession sharedSession] dataTaskWithRequest:request
                                     completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (error != nil) {
                if([weakSelf.delegate respondsToSelector:@selector(errorGetBookDetailByIsbn:error:)]) {
                    [weakSelf.delegate errorGetBookDetailByIsbn:isbn
                                                          error:error];
                }
                return;
            }
            NSError *err = nil;
            NSDictionary *JSONDictionary = [NSJSONSerialization JSONObjectWithData:data
                                                                           options:0
                                                                             error:&err];
            
            if (err != nil) {
                if([weakSelf.delegate respondsToSelector:@selector(errorGetBookDetailByIsbn:error:)]) {
                    [weakSelf.delegate errorGetBookDetailByIsbn:isbn
                                                          error:err];
                }
                return;
            }
            
            BookDetailModel *model = [BookDetailModel convertFromJsonDictionary:JSONDictionary];
            [weakSelf.delegate receivedGetBookDetailByIsbn:isbn
                                                      data:model];
        });
    }] resume];
}

@end
