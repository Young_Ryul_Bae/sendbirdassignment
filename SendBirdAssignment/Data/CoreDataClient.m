#import "CoreDataClient.h"
#import "BookDetailT+CoreDataProperties.h"
#import "BookDetailModel.h"

@interface CoreDataClient ()

@property (strong, nonatomic) NSPersistentContainer *container;

@end

@implementation CoreDataClient

-(instancetype)initWithContainer:(NSPersistentContainer *)container {
    if(self = [super init]) {
        self.container = container;
        _context = self.container.viewContext;
    }
    return self;
}

- (NSArray<BookDetailT *> *)fetchAllBookDetailTWhereNoteIsNotNil {
    if(self.context == nil) {
        return nil;
    }
    
    NSFetchRequest *request = [BookDetailT fetchRequest];
    request.predicate = [NSPredicate predicateWithFormat:@"usernote != nil"];
    NSError *error = nil;
    NSArray<BookDetailT *>* result = [self.context executeFetchRequest:request error:&error];
    
    if([result count] < 1) {
        return @[];
    }
    return result;
}

- (BookDetailT *)fetchBookDetailByIsbn13:(NSString *)isbn13 {
    if(self.context == nil) {
        return nil;
    }
    
    NSFetchRequest *request = [BookDetailT fetchRequest];
    request.predicate = [NSPredicate predicateWithFormat:@"isbn13 = %@", isbn13];
    NSError *error = nil;
    NSArray<BookDetailT *>* result = [self.context executeFetchRequest:request error:&error];
    
    if([result count] < 1) {
        return nil;
    }
    return result[0];
}

- (BOOL)insertBookDetail:(BookDetailModel *)model {
    if(self.context == nil) {
        return NO;
    }
    BookDetailT *book = [NSEntityDescription insertNewObjectForEntityForName:@"BookDetailT"
                                                      inManagedObjectContext:self.context];
    [book setValue:model.authors forKey:@"authors"];
    [book setValue:model.desc forKey:@"desc"];
    [book setValue:model.error forKey:@"error"];
    [book setValue:model.image forKey:@"image"];
    [book setValue:model.isbn10 forKey:@"isbn10"];
    [book setValue:model.isbn13 forKey:@"isbn13"];
    [book setValue:model.language forKey:@"language"];
    [book setValue:model.pages forKey:@"pages"];
    [book setValue:model.price forKey:@"price"];
    [book setValue:model.publisher forKey:@"publisher"];
    [book setValue:model.rating forKey:@"rating"];
    [book setValue:model.subtitle forKey:@"subtitle"];
    [book setValue:model.title forKey:@"title"];
    [book setValue:model.url forKey:@"url"];
    [book setValue:model.year forKey:@"year"];
    
    [self saveContext];
    return YES;
}

- (BOOL)insertUserNote:(NSString *)note toIsbn13:(NSString *)isbn13 {
    if(self.context == nil) {
        return NO;
    }
    
    NSFetchRequest *request = [BookDetailT fetchRequest];
    request.predicate = [NSPredicate predicateWithFormat:@"isbn13 = %@", isbn13];
    NSError *error = nil;
    NSArray<BookDetailT *>* result = [self.context executeFetchRequest:request error:&error];
    if([result count] < 1) {
        return NO;
    }
    
    result[0].usernote = note;
    [self saveContext];
    return YES;
}

- (BOOL)deleteBookDetailByIsbn:(NSString *)isbn13 {
    if(self.context == nil) {
        return NO;
    }
    
    BookDetailT *book = [self fetchBookDetailByIsbn13:isbn13];
    if(book == nil) {
        return NO;
    }
    
    [self.context deleteObject:book];
    [self saveContext];
    return YES;
}

- (void)saveContext {
    NSError *error = nil;
    if ([[self context] save:&error] == NO) {
        NSAssert(NO, @"Error saving context: %@\n%@", [error localizedDescription], [error userInfo]);
    }
}

@end
