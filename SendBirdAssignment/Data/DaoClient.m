#import "DaoClient.h"
#import "ApiClient.h"
#import "CoreDataClient.h"
#import "BookDetailT+CoreDataProperties.h"
#import "SearchModel.h"
#import "BookDetailModel.h"

@interface DaoClient ()

@property (strong, nonatomic) NSMutableDictionary<NSString *, SearchModel *> *cache;
@property (strong, nonatomic) ApiClient *api;
@property (strong, nonatomic) CoreDataClient *coreData;

@end

@implementation DaoClient

+ (NSInteger)defaultPageSize {
    return 10;
}

- (NSString *)buildKeyForCacheByKeyword:(NSString *)keyword
                                andPage:(NSInteger)page {
    return [NSString stringWithFormat:@"%@||%ld", keyword, (long)page];
}

- (instancetype)initWithContainer:(NSPersistentContainer *)container {
    if(self = [super init]) {
        self.cache = [[NSMutableDictionary alloc] init];
        self.api = [[ApiClient alloc] init];
        self.api.delegate = self;
        self.coreData = [[CoreDataClient alloc] initWithContainer: container];
    }
    return self;
}

- (DataOrigin)getBooksByKeyword:(NSString *)keyword
                           page:(NSInteger)page {
    if(![self.delegate respondsToSelector:@selector(receivedGetBooksByKeyword:page:data:)]
       || keyword == nil) {
        return DataOriginNone;
    }
    NSString *key = [self buildKeyForCacheByKeyword:keyword
                                            andPage:page];
    if([self.cache objectForKey:key] != nil) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.delegate receivedGetBooksByKeyword:keyword
                                                page:page
                                                data:[self.cache objectForKey:key]];
        });
        return DataOriginFromInMemory;
    }
    else {
        [self.api getBooksByKeyword:keyword
                               page:page];
        return DataOriginFromWebApi;
    }
}

- (DataOrigin)getBookDetailTByIsbn:(NSString *)isbn {
    if(![self.delegate respondsToSelector:@selector(receivedGetBookDetailTByIsbn:data:)]
       || ![self respondsToSelector:@selector(receivedGetBookDetailByIsbn:data:)]
       || isbn == nil) {
        return DataOriginNone;
    }
    BookDetailT *detail = [self.coreData fetchBookDetailByIsbn13:isbn];
    if(detail != nil) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.delegate receivedGetBookDetailTByIsbn:isbn
                                                   data:detail];
        });
        return DataOriginFromCoreData;
    }
    else {
        [self.api getBookDetailByIsbn:isbn];
        return DataOriginFromWebApi;
    }
}

- (void)getAllBookDetailT {
    if(![self.delegate respondsToSelector:@selector(receivedGetAllBookDetailT:)]) {
        return;
    }
    
    NSArray<BookDetailT *> *data = [self.coreData fetchAllBookDetailTWhereNoteIsNotNil];
    if(data == nil) {
        data = @[];
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.delegate receivedGetAllBookDetailT:data];
    });
}

- (BOOL)saveBookDetail:(BookDetailModel *)bookDetail {
    return [self.coreData insertBookDetail:bookDetail];
}

- (BOOL)saveUserNote:(NSString *)note
            toIsbn13:(NSString *)isbn13 {
    if(isbn13 == nil) {
        return NO;
    }
    BookDetailT *detail = [self.coreData fetchBookDetailByIsbn13:isbn13];
    if(detail != nil) {
        return [self.coreData insertUserNote:note
                                    toIsbn13:detail.isbn13];
    }
    else {
        return NO;
    }
}

- (BOOL)removeBookDetailByIsbn13:(NSString *)isbn13 {
    if(isbn13 == nil) {
        return NO;
    }
    BookDetailT *detail = [self.coreData fetchBookDetailByIsbn13:isbn13];
    if(detail != nil) {
        return [self.coreData deleteBookDetailByIsbn:isbn13];
    }
    else {
        return NO;
    }
}

- (void)receivedGetBooksByKeyword:(NSString *)keyword
                             page:(NSInteger)page
                             data:(SearchModel *)data {
    if(![data.error isEqualToString:@"0"] && [self.delegate respondsToSelector:@selector(errorGetBooksByKeyword:page:error:)]) {
        [self errorGetBooksByKeyword:keyword
                                page:page
                               error:[NSError errorWithDomain:[NSString stringWithFormat:@"custom error %@", data.error]
                                                         code:1
                                                     userInfo:nil] ];
        return;
    }
    
    if(![self.delegate respondsToSelector:@selector(receivedGetBooksByKeyword:page:data:)]) {
        return;
    }
    
    [self.cache setValue:data
                  forKey:[self buildKeyForCacheByKeyword:keyword andPage:page]];
    [self.delegate receivedGetBooksByKeyword:keyword
                                        page:page
                                        data:data];
}

- (void)errorGetBooksByKeyword:(NSString *)keyword
                          page:(NSInteger)page
                         error:(NSError *)error {
    if(![self.delegate respondsToSelector:@selector(errorGetBooksByKeyword:page:error:)]) {
        return;
    }
    [self.delegate errorGetBooksByKeyword:keyword
                                     page:page
                                    error:error];
    
}

- (void)receivedGetBookDetailByIsbn:(NSString *)isbn
                               data:(BookDetailModel *)data {
    if(![data.error isEqualToString:@"0"] && [self.delegate respondsToSelector:@selector(errorGetBookDetailByIsbn:error:)]) {
        [self errorGetBookDetailByIsbn:isbn
                                 error:[NSError errorWithDomain:[NSString stringWithFormat:@"custom error %@", data.error]
                                                           code:1
                                                       userInfo:nil] ];
        return;
    }
    
    if(![self.delegate respondsToSelector:@selector(receivedGetBookDetailTByIsbn:data:)]) {
        return;
    }
    [self saveBookDetail:data];
    BookDetailT *detail = [self.coreData fetchBookDetailByIsbn13:isbn];
    [self.delegate receivedGetBookDetailTByIsbn:isbn
                                           data:detail];
}

- (void)errorGetBookDetailByIsbn:(NSString *)isbn
                           error:(NSError *)error {
    if(![self.delegate respondsToSelector:@selector(errorGetBookDetailByIsbn:error:)]) {
        return;
    }
    [self.delegate errorGetBookDetailByIsbn:isbn
                                      error:error];
}

@end
