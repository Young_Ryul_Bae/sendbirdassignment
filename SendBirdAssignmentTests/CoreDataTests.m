#import <XCTest/XCTest.h>
#import "CoreDataClient.h"
#import "BookDetailT+CoreDataProperties.h"
#import "BookDetailModel.h"

@interface CoreDataTests : XCTestCase

@property (strong, nonatomic) CoreDataClient *cache;
@property (strong, nonatomic) BookDetailModel *stubData;

@end

@implementation CoreDataTests

- (void)setUp {
    // in-memory NSPersistentContainer for test cases
    NSPersistentContainer *inMemoryContainer = [[NSPersistentContainer alloc] initWithName:@"SendBirdAssignment"];
    NSPersistentStoreDescription *desc = [[NSPersistentStoreDescription alloc] init];
    desc.type = NSInMemoryStoreType;
    desc.shouldAddStoreAsynchronously = NO;
    inMemoryContainer.persistentStoreDescriptions = @[desc];
    [inMemoryContainer loadPersistentStoresWithCompletionHandler:^(NSPersistentStoreDescription *desc, NSError *err) {
        XCTAssertEqual(desc.type, NSInMemoryStoreType);
        XCTAssertNil(err);
    }];
    
    // DI
    self.cache = [[CoreDataClient alloc] initWithContainer:inMemoryContainer];
    
    self.stubData = [[BookDetailModel alloc] init];
    self.stubData.authors = @"test_authors";
    self.stubData.desc = @"test_desc";
    self.stubData.error = @"test_error";
    self.stubData.image = @"test_image";
    self.stubData.isbn10 = @"test_isbn10";
    self.stubData.isbn13 = @"test_isbn13";
    self.stubData.language = @"test_language";
    self.stubData.pages = @"test_pages";
    self.stubData.price = @"test_price";
    self.stubData.publisher = @"test_publisher";
    self.stubData.rating = @"test_rating";
    self.stubData.subtitle = @"test_subtitle";
    self.stubData.title = @"test_title";
    self.stubData.url = @"test_url";
    self.stubData.year = @"test_year";
}

- (void)tearDown {
}

- (void)testInsertFetch {
    [self.cache insertBookDetail:self.stubData];
    BookDetailT *fetched = [self.cache fetchBookDetailByIsbn13:self.stubData.isbn13];
    XCTAssertEqual(fetched.authors, self.stubData.authors);
    XCTAssertEqual(fetched.desc, self.stubData.desc);
    XCTAssertEqual(fetched.error, self.stubData.error);
    XCTAssertEqual(fetched.image, self.stubData.image);
    XCTAssertEqual(fetched.isbn10, self.stubData.isbn10);
    XCTAssertEqual(fetched.isbn13, self.stubData.isbn13);
    XCTAssertEqual(fetched.language, self.stubData.language);
    XCTAssertEqual(fetched.pages, self.stubData.pages);
    XCTAssertEqual(fetched.price, self.stubData.price);
    XCTAssertEqual(fetched.publisher, self.stubData.publisher);
    XCTAssertEqual(fetched.rating, self.stubData.rating);
    XCTAssertEqual(fetched.subtitle, self.stubData.subtitle);
    XCTAssertEqual(fetched.title, self.stubData.title);
    XCTAssertEqual(fetched.url, self.stubData.url);
    XCTAssertEqual(fetched.year, self.stubData.year);
    [self.cache deleteBookDetailByIsbn:self.stubData.isbn13];
    fetched = [self.cache fetchBookDetailByIsbn13:self.stubData.isbn13];
    XCTAssertNil(fetched);
}

- (void)testFetchAllWhereNoteIsNotEmpty {
    [self.cache insertBookDetail:self.stubData];
    [self.cache insertUserNote:@"user_note"
                      toIsbn13:self.stubData.isbn13];
    NSArray<BookDetailT *> *fetchedAll = [self.cache fetchAllBookDetailTWhereNoteIsNotNil];
    XCTAssertTrue([fetchedAll count] == 1);
    [self.cache deleteBookDetailByIsbn:self.stubData.isbn13];
    [self.cache insertBookDetail:self.stubData];
    fetchedAll = [self.cache fetchAllBookDetailTWhereNoteIsNotNil];
    XCTAssertTrue([fetchedAll count] == 0);
}

- (void)testInsertAndDelete {
    [self.cache insertBookDetail:self.stubData];
    [self.cache deleteBookDetailByIsbn:self.stubData.isbn13];
    BookDetailT *fetched = [self.cache fetchBookDetailByIsbn13:self.stubData.isbn13];
    XCTAssertNil(fetched);
}

- (void)testInsertUserNote {
    NSString *testNote = @"test_note";
    
    [self.cache insertBookDetail:self.stubData];
    [self.cache insertUserNote:testNote
                      toIsbn13:self.stubData.isbn13];
    
    BookDetailT *fetched = [self.cache fetchBookDetailByIsbn13:self.stubData.isbn13];
    XCTAssertEqual(testNote, fetched.usernote);
}

@end
