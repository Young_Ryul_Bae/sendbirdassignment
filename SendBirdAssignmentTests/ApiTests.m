#import <XCTest/XCTest.h>
#import "ApiClient.h"
#import "SearchModel.h"
#import "BookDetailModel.h"

@interface ApiTests : XCTestCase <ApiClientDelegate>

@property(strong, nonatomic) XCTestExpectation *expectation;
@property(strong, nonatomic) SearchModel *searchModel;
@property(strong, nonatomic) BookDetailModel *bookDetailModel;

@end

@implementation ApiTests

- (void)setUp {
}

- (void)tearDown {
}

- (void)testSearchApi {
    NSString *keyword = @"apple";
    NSInteger page = 2;

    ApiClient *api = [[ApiClient alloc] init];
    api.delegate = self;
    [api getBooksByKeyword:keyword
                      page:page];
    self.expectation = [[XCTestExpectation alloc] initWithDescription:@"testSearchApi api"];
    [self waitForExpectations:@[self.expectation] timeout:3];
    
    XCTAssertNotNil(self.searchModel);
    XCTAssertGreaterThan([self.searchModel.total intValue], 0);
    printf("\n\n-------------------\ntestlog<testSearchApi>:\n%s\n------------------\n\n",
           [self.searchModel.description UTF8String]);
}

- (void)testSearchApiWithLargeSizePage {
    NSString *keyword = @"how scissors work";
    NSInteger largeSizePage = 300;
    
    ApiClient *api = [[ApiClient alloc] init];
    api.delegate = self;
    [api getBooksByKeyword:keyword
                      page:largeSizePage];
    self.expectation = [[XCTestExpectation alloc] initWithDescription:@"testSearchApiWithLargeSizePage api"];
    [self waitForExpectations:@[self.expectation] timeout:3];
    
    XCTAssertNotNil(self.searchModel);
    XCTAssertGreaterThanOrEqual([self.searchModel.page intValue], largeSizePage);
    printf("\n\n-------------------\ntestlog<testSearchApiWithLargeSizePage>:\n%s\n------------------\n\n",
    [self.searchModel.description UTF8String]);
}


- (void)testDetailApi {
    NSString *isbn = @"9781849693806";
    
    self.expectation = [[XCTestExpectation alloc] initWithDescription:@"testDetailApi api"];
    ApiClient *api = [[ApiClient alloc] init];
    api.delegate = self;
    [api getBookDetailByIsbn:isbn];
    [self waitForExpectations:@[self.expectation] timeout:3];
    
    XCTAssertNotNil(self.bookDetailModel);
    XCTAssertTrue([isbn isEqualToString:self.bookDetailModel.isbn13]);
    printf("\n\n-------------------\ntestlog<testDetailApi>:\n%s\n------------------\n\n",
           [self.bookDetailModel.description UTF8String]);
}

- (void)testDetailApiWrongIsbn {
    NSString *isbn = @"ASO&!^23";
    
    self.expectation = [[XCTestExpectation alloc] initWithDescription:@"testDetailApiWrongIsbn api"];
    ApiClient *api = [[ApiClient alloc] init];
    api.delegate = self;
    [api getBookDetailByIsbn:isbn];
    [self waitForExpectations:@[self.expectation] timeout:3];
    printf("\n\n-------------------\ntestlog<testDetailApiWrongIsbn>:\n%s\n------------------\n\n",
           [self.bookDetailModel.description UTF8String]);
}

- (void)errorGetBookDetailByIsbn:(NSString *)isbn
                           error:(NSError *)error {
    [self.expectation fulfill];
}

- (void)receivedGetBooksByKeyword:(NSString *)keyword
                             page:(NSInteger)page
                             data:(SearchModel *)data {
    self.searchModel = data;
    [self.expectation fulfill];
}

- (void)receivedGetBookDetailByIsbn:(NSString *)isbn
                               data:(BookDetailModel *)data {
    self.bookDetailModel = data;
    [self.expectation fulfill];
}

@end
