#import <XCTest/XCTest.h>
#import "DaoClient.h"
#import "BookDetailT+CoreDataProperties.h"

@interface DaoClientTests : XCTestCase <DaoClientDelegate>

@property(strong, nonatomic) DaoClient *dao;
@property(strong, nonatomic) XCTestExpectation *expectation;
@property(strong, nonatomic) SearchModel *searchModel;
@property(strong, nonatomic) BookDetailT *bookDetailT;

@end

@implementation DaoClientTests

- (void)setUp {
    NSPersistentContainer *inMemoryContainer = [[NSPersistentContainer alloc] initWithName:@"SendBirdAssignment"];
    NSPersistentStoreDescription *desc = [[NSPersistentStoreDescription alloc] init];
    desc.type = NSInMemoryStoreType;
    desc.shouldAddStoreAsynchronously = NO;
    inMemoryContainer.persistentStoreDescriptions = @[desc];
    [inMemoryContainer loadPersistentStoresWithCompletionHandler:^(NSPersistentStoreDescription *desc, NSError *err) {
        XCTAssertEqual(desc.type, NSInMemoryStoreType);
        XCTAssertNil(err);
    }];
    self.dao = [[DaoClient alloc] initWithContainer:inMemoryContainer];
    self.dao.delegate = self;
}

- (void)tearDown {
}

- (void)testGetSearchFromInMemory {
    NSString *keyword = @"apple";
    NSInteger page = 1;
    
    DataOrigin origin = [self.dao getBooksByKeyword:keyword
                                               page:page];
    XCTAssertEqual(origin, DataOriginFromWebApi);
    
    self.expectation = [[XCTestExpectation alloc] initWithDescription:@"testGetSearchFromInMemory api"];
    [self waitForExpectations:@[self.expectation] timeout:3];
    origin = [self.dao getBooksByKeyword:keyword
                                    page:page];
    
    XCTAssertEqual(origin, DataOriginFromInMemory);
}

- (void)testSaveAndGetBookDetailFromCoreData {
    NSString *isbn = @"9781849693806";
    
    DataOrigin origin = [self.dao getBookDetailTByIsbn:isbn];
    XCTAssertEqual(origin, DataOriginFromWebApi);
    
    self.expectation = [[XCTestExpectation alloc] initWithDescription:@"testSaveAndGetBookDetailFromCoreData api"];
    [self waitForExpectations:@[self.expectation] timeout:3];
    origin = [self.dao getBookDetailTByIsbn:isbn];
    
    XCTAssertEqual(origin, DataOriginFromCoreData);
    XCTAssertTrue([isbn isEqualToString:self.bookDetailT.isbn13]);
}

- (void)testSaveUserNote {
    NSString *isbn = @"9781484231319";
    NSString *note = @"test_note";
    
    [self.dao getBookDetailTByIsbn:isbn];
    self.expectation = [[XCTestExpectation alloc] initWithDescription:@"testSaveUserNote api"];
    [self waitForExpectations:@[self.expectation] timeout:3];
    [self.dao saveUserNote:note
                  toIsbn13:isbn];
    DataOrigin origin = [self.dao getBookDetailTByIsbn:isbn];
    
    XCTAssertEqual(origin, DataOriginFromCoreData);
    XCTAssertTrue([isbn isEqualToString:self.bookDetailT.isbn13]);
    XCTAssertTrue([note isEqualToString:self.bookDetailT.usernote]);
}

- (void)testRemoveBookDetail {
    NSString *isbn = @"9781934356326";
    
    [self.dao getBookDetailTByIsbn:isbn];
    self.expectation = [[XCTestExpectation alloc] initWithDescription:@"testRemoveBookDetail api"];
    [self waitForExpectations:@[self.expectation] timeout:3];
    DataOrigin origin = [self.dao getBookDetailTByIsbn:isbn];
    XCTAssertEqual(origin, DataOriginFromCoreData);
    [self.dao removeBookDetailByIsbn13:isbn];
    origin = [self.dao getBookDetailTByIsbn:isbn];
    XCTAssertEqual(origin, DataOriginFromWebApi);
}

- (void)receivedGetBooksByKeyword:(NSString *)keyword
                             page:(NSInteger)page
                             data:(SearchModel *)data {
    self.searchModel = data;
    [self.expectation fulfill];
}

- (void)receivedGetBookDetailTByIsbn:(NSString *)isbn
                                data:(BookDetailT *)data {
    self.bookDetailT = data;
    [self.expectation fulfill];
}

@end
