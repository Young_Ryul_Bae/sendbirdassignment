# Added Features
I'll skip the requirement features in PDF and go straight to the features that I build with my own thoughts.

## Cache
In most cases, caching data by raw string keyword is not a good idea because
- Hit rate will be extremely low
- Could easily lead to the memory overflow

However, this is not a product-level project so I decided to make a in-memory cache(**NSDictionary<NSString *, SearchModel *>**) which the key is concatenation of search keyword and page number. 

Moreover, Image files were big enough to cause blinking & lagging when scrolling down the UICollectionview so I also made a in-memory cache(**NSDictionary<NSString *, UIImageView *>**) to quicken the process of the image data. 

Detail book infos by ISBN seemed quite static and "cachable" for me so it was stored to Core Data. Plus, one of the requirements was saving user's note so I append the usernote column to the Core Data.

## API
API document was not provided and indeed, it was error prone so I had to catch the exceptions and loosen the test cases.

## MyNoteViewController
The users will want to revise there notes so I made a  dedicated VC which lists the books which has the user note.

## Small Things...
- Heavy .h files make build slow, clunky and unsafe if a lot of programmers are working on same project. I write the "public" methods and properties as small as I can.
-  I believe user experience is one of core part of app programming so I took my extra time to improve it.

